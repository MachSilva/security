-- Consultas especificadas em doc/trabalho.pdf
-- Seção Consultas e Operações em SQL

-- Liste o nome do trangressor, a hora e o tipo de todos os crimes que ocorreram entre as datas X e Y :
-- /*
set @X = '2017-01-01';
set @Y = '2017-12-01';

select nome_completo, data_hora, natureza from
(
	(
		select data_hora, cpf from crime
		where data_hora between @X and @Y
	) as A
	join
	crime_natureza as C
	on (A.data_hora = C.crime_data_hora and A.cpf = C.crime_cpf)
	join
	pessoa as P
	on (A.cpf = P.cpf) 
);
-- */

-- Dada uma unidade policial X (X é o id da unidade), liste suas investigações com ao menos um depoimento,
-- os responsáveis por estas investigações e a quantidade de depoimentos prestados a mesma:
-- /*
set @X = 1;
select matricula, investigacao, no_depoimentos from (
	(select investigacao, count(investigacao) as no_depoimentos from depoimento group by investigacao) as D
	join
	(select investigacao as inv, responsavel from depoimento) as E
	on (D.investigacao = E.inv)
	join
	(select matricula from oficial where unidade = @X) as F
	on (E.responsavel = F.matricula)
);
-- */

-- Dado o \emph{CPF X} de uma pessoa, mostre seu nome e sua ficha criminal
-- (hora do crime e descrição) caso exista:
-- /*
set @X = 906;
select * from (
	(select cpf, nome_completo from pessoa) as P
	join
	crime as C
	on P.cpf = C.cpf
);
-- */

-- Dado um conjunto $S = \{s_1, ..., s_n\}$ de naturezas possíveis de ocorrências (homicídio, assalto, etc.), onde $s_i$ é uma relação e $s_i \in \pi_{nome} (\text{natureza\_ocorrencia})$, liste os crimes que se encaixam em \emph{todas} as categorias em $S$:
-- S = ("CONSUMADO", "HOMICIDIO")
-- |S| = 2

select cpf, data_hora, descricao from (
	crime as C
	join
	(
		select crime_cpf, crime_data_hora, count(*) from crime_natureza
		where natureza in ("CONSUMADO", "HOMICIDIO")
		group by crime_cpf, crime_data_hora
		having count(*) = 2
	) as A
	on (C.data_hora = A.crime_data_hora and C.cpf = A.crime_cpf)
);

-- Dado um bairro B, liste a quantidade de pessoas desaparecidas que foram vistas alguma vez neste bairro (ou seja, quantas pessoas desapareceram):
set @B = 'Limoeiro';

select count(cpf) from (
	desaparecido as D
	join
	(
		select id, bairro from local where bairro = @B
	) as A
	on D.visto_por_ultimo = A.id
);
