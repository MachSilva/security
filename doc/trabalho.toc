\changetocdepth {4}
\babel@toc {brazil}{}
\babel@toc {brazil}{}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Especifica\IeC {\c c}\IeC {\~a}o do Problema}}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Introdu\IeC {\c c}\IeC {\~a}o e descri\IeC {\c c}\IeC {\~a}o do mini-mundo}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Especifica\IeC {\c c}\IeC {\~a}o do Problema}{4}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Objetivos da modelagem}{4}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Caracter\IeC {\'\i }sticas dos tipos-entidade e dos seus atributos}{4}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Funcionalidades dos tipos-relacionametos}{7}{subsection.1.2.3}
\contentsline {subsection}{\numberline {1.2.4}Principais consultas}{10}{subsection.1.2.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Esquemas Conceituais e Relacionais}}{11}{chapter.2}
\contentsline {section}{\numberline {2.1}Esquema Conceitual}{11}{section.2.1}
\contentsline {section}{\numberline {2.2}Esquema Relacional}{12}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Rela\IeC {\c c}\IeC {\~o}es j\IeC {\'a} normalizadas}{13}{subsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.1.1}1FN ou Primeira Forma Normal}{13}{subsubsection.2.2.1.1}
\contentsline {subsubsection}{\numberline {2.2.1.2}2FN ou Segunda Forma Normal}{13}{subsubsection.2.2.1.2}
\contentsline {subsubsection}{\numberline {2.2.1.3}3FN ou Terceira Forma Normal}{14}{subsubsection.2.2.1.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Consultas e Implementa\IeC {\c c}\IeC {\~a}o}}{16}{chapter.3}
\contentsline {section}{\numberline {3.1}Consultas em \IeC {\'A}lgebra Relacional}{16}{section.3.1}
\contentsline {section}{\numberline {3.2}Cria\IeC {\c c}\IeC {\~a}o do Banco de Dados}{17}{section.3.2}
\contentsline {section}{\numberline {3.3}Consultas e Opera\IeC {\c c}\IeC {\~o}es em \emph {SQL}}{23}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Consultas}{23}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Opera\IeC {\c c}\IeC {\~o}es}{26}{subsection.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.2.1}Inser\IeC {\c c}\IeC {\~a}o}{26}{subsubsection.3.3.2.1}
\contentsline {subsubsection}{\numberline {3.3.2.2}Remo\IeC {\c c}\IeC {\~a}o}{27}{subsubsection.3.3.2.2}
\contentsline {subsubsection}{\numberline {3.3.2.3}Atualiza\IeC {\c c}\IeC {\~a}o}{27}{subsubsection.3.3.2.3}
\contentsline {section}{\numberline {3.4}Implementa\IeC {\c c}\IeC {\~a}o}{28}{section.3.4}
