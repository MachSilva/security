# Trabalho de Banco de Dados

Tema: **Segurança no Brasil**

O enunciado e o *PDF* a ser entregue no T1 e T2 está na pasta *doc*.
Também na mesma pasta estará o esquema relacional do Banco de Dados. Recomendo o site [draw.io](https://www.draw.io) para isso.

## Como gerar o *pdf*

Pré-requesito: instalar pacotes com *abntex2* e o comando *pdflatex*
* `$ sudo apt install texlive texlive-lang-portuguese texlive-publishers`

Gerar *pdf*:
* `$ pdflatex trabalho.tex`

## Entrega e Resumo das seções

### T1 (19/03) em *pdf*
* *3.1*: Capa
* *3.2*: Índice
* *3.3*: Especificação do Problema; entidades; relações; principais consultas
* *3.4*: Esquema Conceitual; recomendo [draw.io](https://www.draw.io)

### T2 (23/04) em *pdf*
* *3.5*: Esquema Relacional
* *3.6*: Normalização

### T3 (21/05) em *zip* e apresentação
* *3.7*: Especificação de Consultas em Álgebra Relacional
* *3.8*: Criação do Banco de Dados
* *3.9*: Especificação de Consultas em SQL
* *3.10*: Implementação

# Instalação

Todos os procedimentos a seguir foram testado em distribuições *Ubuntu-based*.

## Dependências

Os pacotes necessários a execução deste software podem ser instalados desta forma:

* `$ sudo apt install mysql-server-5.7 apache2 php7.2 php7.2-mysql`

## Configuração

* A completar

