<?php 

session_start();

include '__app/Config.inc.php';

if(CheckLogin()):
	header('Location: index.php');
endif;

$GetPost = filter_input(INPUT_POST, 'login', FILTER_DEFAULT);
$PostData = filter_input_array(INPUT_POST, FILTER_DEFAULT);

if($PostData):
	$PostData = array_map('strip_tags', $PostData);

	$Username = $PostData['matricula'];
	$Userpass = md5($PostData['senha']);

	$Read = new Read();
	$Read->ExeRead(TB_OFICIAL, "WHERE matricula = :e AND senha = :f", "e={$Username}&f={$Userpass}");

	if($Read->getResult()):
		$_SESSION['userlogin'] = $Read->getResult()[0];
		header('Location: index.php');
	else:
		?>
		<script type="text/javascript">
			window.alert("Dados Inconpatíveis");
		</script>
		<?php
	endif;

endif;

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?= SITENAME; ?></title>
	<link type="text/css" href="<?= HOME; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link type="text/css" href="<?= HOME; ?>/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link type="text/css" href="<?= HOME; ?>/css/theme.css" rel="stylesheet">
	<link type="text/css" href="<?= HOME; ?>/images/icons/css/font-awesome.css" rel="stylesheet">
</head>
<body>

	<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
					<i class="icon-reorder shaded"></i>
				</a>

			  	<a class="brand" href="<?= HOME; ?>">
			  		<?= SITENAME; ?>
			  	</a>
			</div>
		</div><!-- /navbar-inner -->
	</div><!-- /navbar -->



	<div class="wrapper">
		<div class="container">
			<div class="row">
				<div class="module module-login span4 offset4">
					<form class="form-vertical" method="post">
						<div class="module-head">
							<h3>Entrar</h3>
						</div>
						<div class="module-body">
							<div class="control-group">
								<div class="controls row-fluid">
									<input class="span12" type="text" name="matricula" placeholder="Matricula">
								</div>
							</div>
							<div class="control-group">
								<div class="controls row-fluid">
									<input class="span12" type="password" name="senha" placeholder="Senha">
								</div>
							</div>
						</div>
						<div class="module-foot">
							<div class="control-group">
								<div class="controls clearfix">
									<button type="submit" class="btn btn-primary pull-right">Entrar</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div><!--/.wrapper-->

	<div class="footer">
		<div class="container">
			<b class="copyright">&copy; 2018 <?= SITENAME ?> - </b>Todos os direitos reservados.
		</div>
	</div>
</body>