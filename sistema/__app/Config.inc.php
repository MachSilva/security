<?php
// CONFIGRAÇÕES DO BANCO ####################
const HOST = 'localhost';
const USER = 'root';
const PASS = '';
const DBSA = 'security';

// DEFINE IDENTIDADE DO SITE ################
const SITENAME = "Security";
const SITEDESC = null;

// DEFINE A BASE DO SITE ####################
const HOME  = 'http://localhost/banco';

// TB NAMES
const TB_BOLETIM      = 'boletim';
const TB_CRIMES       = 'crime';
const TB_DESAPARECIDO = 'desaparecido';
const TB_INVESTIGACAO = 'investigacao';
const TB_LOCAL        = 'local';
const TB_OFICIAL      = 'oficial';
const TB_OFICIAL_OPER = 'oficial_operacao';
const TB_OPERACAO     = 'operacao';
const TB_PESSOA       = 'pessoa';
const TB_RESIDE       = 'pessoa_reside_em';
const TB_UNIDADE      = 'unidade';

// AUTO LOAD DE CLASSES ####################
function __autoload($Class) {

    $cDir = ['Conn', 'Helpers', 'Models','Controller'];
    $iDir = null;

    foreach ($cDir as $dirName):
        if (!$iDir && file_exists(__DIR__ . DIRECTORY_SEPARATOR . $dirName . DIRECTORY_SEPARATOR . $Class . '.class.php') && !is_dir(__DIR__ . DIRECTORY_SEPARATOR . $dirName . DIRECTORY_SEPARATOR . $Class . '.class.php')):
            include_once (__DIR__ . DIRECTORY_SEPARATOR . $dirName . DIRECTORY_SEPARATOR . $Class . '.class.php');
            $iDir = true;
        endif;
    endforeach;

    if (!$iDir):
        trigger_error("Não foi possível incluir {$Class}.class.php", E_USER_ERROR);
        die;
    endif;

}

// FAZ A CHECAGEM DE LOGIN PARA O PAINEL
function CheckPainel(){
    $Retorno = false;
    if(!$_SESSION['userlogin']){
        unset($_SESSION['userlogin']);
        //header("Location: index.php?exe=restrito");
    }else{
        $ID = $_SESSION['userlogin']['matricula'];
        $read = new Read;
        $read->ExeRead(TB_OFICIAL,"WHERE matricula = :id","id={$ID}");
        if(!$read->getResult()):
            header("Location: index.php?sys=restrito");
            unset($_SESSION['userlogin']);   
            die();
        else:
            $Retorno = true;
        endif;
    }

    return $Retorno;
}

// FAZ A CHECAGEM DO LOGIN PARA A PAGINA DE LOGIN
function CheckLogin(){
    $login = new Login();
    if($login->CheckLogin()):
        return true;
    endif;

    return false;
}