<?php

/**
 * IncScripts.class [HELPER]
 * Responsável por incluir arquivos de js e css
 *
 * @copyright (c) 2016 Marllon Rodrigues TecWeb
 */
class IncScripts{

	private $Data;
	private $Script;
	private $Result;
	private $Base;


	public function IncScript(array $Data, $Url = false){
		$this->Data = $Data;

		foreach ($this->Data as $key => $value) {
			if($Url == true):
				if($value == 'css'):
					$this->Result = "<link rel=\"stylesheet\" type=\"text/css\" href=\"{$key}\">"."\n";
				elseif($value == 'js'):
					$this->Result = "<script src=\"{$key}\" type=\"text/javascript\" charset=\"utf-8\"></script>"."\n";
				endif;
			else:
				if(file_exists($key) && !is_dir($key)):
					if($value == 'css'):
						$this->Result = "<link rel=\"stylesheet\" type=\"text/css\" href=\"{$key}\">"."\n";
					elseif($value == 'js'):
						$this->Result = "<script src=\"{$key}\" type=\"text/javascript\" charset=\"utf-8\"></script>"."\n";
					endif;
				else:
					$this->Result = "<!-- Arquivo {$key} não é válido, ou não existe! -->"."\n";
				endif;
			endif;
		}

		return $this->Result;
	}

}