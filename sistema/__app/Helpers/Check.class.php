<?php 

/**
 * Tools.class [HELPER]
 * Classe responsável por manipular e validar dados do sistema
 * @copyright (c) 2016 Marllon Rodrigues TecWeb
*/
class Tools{

    private static $Data;
    private static $Format;
    private static $Return;
    private static $Result;
    private static $Count;
    private static $Number;


    /**
     * <b>Verifica E-mail:</b> Executa validação de formato de e-mail. Se for um email válido retorna true, ou retorna false.
     * @param STRING $Email = Uma conta de e-mail
     * @return BOOL = True para um email válido, ou false
     */
    public static function Email($Email) {
        self::$Data = (string) $Email;
        self::$Format = '/[a-z0-9_\.\-]+@[a-z0-9_\.\-]*[a-z0-9_\.\-]+\.[a-z]{2,4}$/';

        if (preg_match(self::$Format, self::$Data)):
            return true;
        else:
            return false;
        endif;
    }

    /**
     * <b>Tranforma URL:</b> Tranforma uma string no formato de URL amigável e retorna o a string convertida!
     * @param STRING $Name = Uma string qualquer
     * @return STRING = $Data = Uma URL amigável válida
     */
    public static function Name($Name) {
        self::$Format = array();
        self::$Format['a'] = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜüÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿRr"!@#$%&*()_-+={[}]/?;:.,\\\'<>°ºª';
        self::$Format['b'] = 'aaaaaaaceeeeiiiidnoooooouuuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr                                 ';

        self::$Data = strtr(utf8_decode($Name), utf8_decode(self::$Format['a']), self::$Format['b']);
        self::$Data = strip_tags(trim(self::$Data));
        self::$Data = str_replace(' ', '-', self::$Data);
        self::$Data = str_replace(array('-----', '----', '---', '--'), '-', self::$Data);

        return strtolower(utf8_encode(self::$Data));
    }

    /**
     * <b>Tranforma Data:</b> Transforma uma data no formato DD/MM/YY em uma data no formato TIMESTAMP!
     * @param STRING $Name = Data em (d/m/Y) ou (d/m/Y H:i:s)
     * @return STRING = $Data = Data no formato timestamp!
     */
    public static function Data($Data) {
        self::$Format = explode(' ', $Data);
        self::$Data = explode('/', self::$Format[0]);

        if (empty(self::$Format[1])):
            self::$Format[1] = date('H:i:s');
        endif;

        self::$Data = self::$Data[2] . '-' . self::$Data[1] . '-' . self::$Data[0] . ' ' . self::$Format[1];
        return self::$Data;
    }

    /**
     * [DataSemana: Retorna em que dia da semana uma certa data caiu]
     * @param [DATA] $Data [Data a ser pesquisada: (d/m/Y)]
     * @param [STRING] $Type [Se holver algo inserido neste campo, o valor retornado será um numero de 1-7, referente a semana]
     * @return $Result = Será retornado o nome do dia da semana ou o número equivalente. 
     */
    public static function DataSemana($Data, $Type = null) {
        self::$Data = explode('/', $Data);

        $diasemana = date("w", mktime(0,0,0,self::$Data[1],self::$Data[0],self::$Data[2]) );    
        switch($diasemana) {        
            case"0": self::$Return = ["Domingo",        "1"]; break;        
            case"1": self::$Return = ["Segunda-Feira",  "2"]; break;        
            case"2": self::$Return = ["Terça-Feira",    "3"]; break;        
            case"3": self::$Return = ["Quarta-Feira",   "4"]; break;        
            case"4": self::$Return = ["Quinta-Feira",   "5"]; break;        
            case"5": self::$Return = ["Sexta-Feira",    "6"]; break;        
            case"6": self::$Return = ["Sábado",         "7"]; break;    
        }   

        self::$Result = ($Type == null ? self::$Return[0] : self::$Return[1]); 
        return self::$Result;

    }

    /**
     * [DataRelatorio: Faz o retorno das datas com o horario inicial e final]
     * @param [DATA] $Data [Data a ser inserida (d/m/Y)]
     * @param [STRING] $Type [Tipo que a ser inserido: "ini" inicial, "fim" hora final]
     * @return $Data = Retorna a data em TIMESTAMP
     */
    public static function DataRelatorio($Data, $Type) {
        self::$Format = explode(' ', $Data);
        self::$Data = explode('/', self::$Format[0]);

        if (empty(self::$Format[1])):
            if($Type == 'ini'):
                self::$Format[1] = '00:00:00';
            else:
                self::$Format[1] = '23:59:59';
            endif;
        endif;

        self::$Data = self::$Data[2] . '-' . self::$Data[1] . '-' . self::$Data[0] . ' ' . self::$Format[1];
        return self::$Data;
    }

    /**
     * [CountDate Conta quantos dias ocorreu entre duas datas]
     * @param [ARRAY] $Data [informe um array contendo duas datas]
     *                      [as datas deve ter ["Data Base", "Data Final"]]
     * @return $Return = Retorna o valor de dias que aconteceu entre as duas datas informadas                     
     */
    public static function CountDate($Data){

        // Verifica se a variável é um array
        if(!is_array($Data)):
            self::$Return = "Você deve informar um array para que seja calculado"; 
        else:
            //Verifica se dentro do array existe uma variável vazia
            if(in_array('', $Data)):
                self::$Return = "Favor informar duas datas em um array";
            else:
                $dt[0] = explode('/', $Data[0]);
                $dt[1] = explode('/', $Data[1]);

                $Final[0] = $dt[0][2]."-".$dt[0][1]."-".$dt[0][0];
                $Final[1] = $dt[1][2]."-".$dt[1][1]."-".$dt[1][0];

                $time[0] = strtotime($Final[0]);
                $time[1] = strtotime($Final[1]);

                $Diference = $time[1] - $time[0];
                self::$Return = (int)floor( $Diference / (60 * 60 * 24));

                // Verifica se o número é negativo
                if(self::$Return < 0):
                    self::$Return = self::$Return * -1;
                endif; 

            endif;
        endif;

        return self::$Return;
    }

    /**
     *<b>Hash:</b> Tool Responsável por criar hashs de senhas ou tokkens.
     * @param STRING $Palavra = Coloque a palavra para ela ser criptografada
     * @return self::$Result = Retorna uma criptografia de 128 caracteres.
    */
    public static function Hash($Palavra){
        
        self::$Data = strip_tags(trim($Palavra));
        self::$Count = (int) strlen(self::$Data);

        self::$Return  = substr(self::$Data, -(self::$Count/2+1), 1);
        self::$Return .= substr(self::$Data, -(self::$Count/2+2), self::$Count*4/10);
        self::$Return .= substr(self::$Data, -(self::$Count/2+3), 1);
        self::$Return .= substr(self::$Data, -(self::$Count/2+4), 2);

        self::$Result = hash('sha512', self::$Return.self::$Data);

        return self::$Result;
    
    }

    /**
     * <b>Limita os Palavras:</b> Limita a quantidade de palavras a serem exibidas em uma string!
     * @param STRING $String = Uma string qualquer
     * @return INT = $Limite = String limitada pelo $Limite
     */
    public static function Words($String, $Limite, $Pointer = null) {
        self::$Data = strip_tags(trim($String));
        self::$Format = (int) $Limite;

        $ArrWords = explode(' ', self::$Data);
        $NumWords = count($ArrWords);
        $NewWords = implode(' ', array_slice($ArrWords, 0, self::$Format));

        $Pointer = (empty($Pointer) ? '...' : ' ' . $Pointer );
        $Result = ( self::$Format < $NumWords ? $NewWords . $Pointer : self::$Data );
        return $Result;
    }

    /**
     * <b>Regra de 3:</b> Ao executar este HELPER, ele automaticamente verifica os valores dos campos e faz
     * matemático e retorna o valor
     * @return NUMERO = retorna o numero calculado
     */
    public static function Regra( $v1, $v2, $v3, $v4 ){
        
        self::$Number[0] = is_numeric($v1);
        self::$Number[1] = is_numeric($v2);
        self::$Number[2] = $v3;
        self::$Number[3] = $v4;

        if(self::$Number[2] == 'x'){
        
            $c[0]         = self::$Number[0]*self::$Number[3];
            $c[1]         = self::$Number[1];
            
            $c[2]         = $c[0] / $c[1];
            self::$Result = $c[2];
        
        }else if(self::$Number[3] == 'x'){
            
            $c[0]         = self::$Number[2]*self::$Number[1];
            $c[1]         = self::$Number[0];
            
            $c[2]         = $c[0] / $c[1];
            self::$Result = $c[2];
        
        }else{
            self::$Result = '';
        }

        return self::$Result;
    }

    /**
    *<b>Money</b> Ao execupar está HELPER, ela irá retornar o valor convertido em dinheio
    * @return NUMBER = Retorna o valor em dinheiro
    */
    public static function FormatMoney($Data = null){
        self::$Data   = (string) $Data;

        self::$Return = (self::$Data == null ? '0.00' : self::$Data);
        self::$Result = "R$ ".number_format(self::$Return, 2, ',','.');

        return self::$Result;
    }

    /**
     * <b>Tags:</b> Ao executar este HELPER, ele irá procurar pelos dados informados e ira
     * criar uma string com os arrays
     * @param $TagsSearch = Insira texto com a separação "," para que possa
     *                      que possa ser pesquisado no database
     * @param $Table = Coloque a tabela em que que se quer pesquisar as IDs das tags
     * @param $Condition = Coloque a condição para a procura
     * @param $NameID = Coloque aqui qual o nome da lina da tabela que identifica as tags
     * 
     * @return ARRAY = retorna um array com tags
     */    
    public static function CreateTags( $TagsSeach, $Table, $Condition, $NameID ){
        
        $Read = new Read;
        $Retorno = "";
        $Tags = "";
        $i = 0;

        // 1° Parte: Ira dar um explode nas tags para separalas
        
        $ExplodeSeach = explode(',', $TagsSeach);

        // 2° Passo: Ira procurar no banco de dados as tags inseridas e retornar os seus respectivos ids
        
        foreach($ExplodeSeach as $tag):
            $Read->ExeRead($Table,"WHERE {$Condition} = :condition","condition={$tag}");
            if($Read->getResult()):
                $Retorno .= $Read->getResult()[0][$NameID] . ",";
            endif;
        endforeach;

        // 3° Passo: Após retornar os identificadores das tags, agora ele irá contar as tags e colocar um limitador no ponto certo
        // isso faz com que na hora da edição da tabela possa ser gerido corretamente os dados
         
        $ExplodeTags = explode(',', $Retorno);
        $CountArray = count($ExplodeTags) - 1;
        unset($ExplodeTags[$CountArray]);
        foreach ($ExplodeTags as $Key) {
            $i++;
            $Delimiter = ($i != $CountArray ? ',' : '');
            $Tags .= "{$Key}{$Delimiter}";
        }

        return $Tags;
    }

    /**
     * <b>getBrowser:</b> Está HELPER é responsável por pegar o navegador do usuário
     * @return STRING = Pega todas as informaçações do navegador do usuário
     */    
    public static function getBrowser(){
        self::$Data = filter_input(INPUT_SERVER, "HTTP_USER_AGENT", FILTER_DEFAULT);
        return self::$Data;
    }

    /**
     * [getNameBrowser responsável por identificar o nome do navegador]
     * @param  [STRING] $Data [Coloque o nome que der na função tools::getBrowser]
     * @return [STRING]       [Retornará apenas o nome do navegador]
     */
    public static function getNameBrowser($Data){
        self::$Data = strip_tags(trim($Data));

        if (strpos(self::$Data, 'Chrome')):
            self::$Data = 'Chrome';
        elseif (strpos(self::$Data, 'Firefox')):
            self::$Data = 'Firefox';
        elseif (strpos(self::$Data, 'MSIE') || strpos(self::$Data, 'Trident/')):
            self::$Data = 'IE';
        else:
            self::$Data = 'Outros';
        endif;

        return self::$Data;
    }

    /**
     * <b>getIP:</b> Está HELPER é responsável por pegar o navegador do usuário
     * @return STRING = Retorna o IP do usuário
     */  
    public static function getIP(){
        self::$Data = filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_VALIDATE_IP);
        return self::$Data;
    }

    /**
     * <b>getBrowser:</b> Está HELPER é responsável por pegar o navegador do usuário
     * @return STRING = Retorna a pagina em que o usuário está acessando
     */  
    public static function getPage(){
        self::$Data = filter_input(INPUT_SERVER, 'REQUEST_URI', FILTER_DEFAULT);
        return self::$Data;
    }

    /**
     * <b>LogUser:</b> Está HELPER é responsável pala criação de logs no bando de dados
     * @param
     * @return BOLEAN = true or false
     */
    public static function LogUser($Mensage, $Table,$UserID){
        
        self::$Data = array(
            "log_user_id" => $UserID,
            "log_mensage" => $Mensage,
            "log_ip"      => Tools::getIP(),
            "log_agent"   => Tools::getBrowser(),
            "log_page"    => Tools::getPage(),
            "log_date"    => Tools::Data(date('d/m/Y H:i:s'))
        );

        if(in_array('',self::Data)):
            self::$Return = false;
        else:

            $Create = new Create;
            $Create->ExeCreate($Table, self::$Data);

            if($Create->getResult()):
                self::$Return = true;
            else:
                self::$Return = false;
            endif;

        endif;

        return self::$Return;
    }

}