﻿<?php 
session_start();
include("__app/Config.inc.php");

if(!CheckPainel()):
    header('Location: login.php');
endif;

$getViewInput = filter_input(INPUT_GET, 'sys', FILTER_DEFAULT);
$getView = ($getViewInput == 'home' ? 'crimes/index' : $getViewInput);

if($getView == 'sair'):
    unset($_SESSION['userlogin']);
    header('Location: login.php');
    die();
endif;

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= SITENAME ?></title>
    <link type="text/css" href="<?= HOME ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link type="text/css" href="<?= HOME ?>/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
    <link type="text/css" href="<?= HOME ?>/css/theme.css" rel="stylesheet">
    <link type="text/css" href="<?= HOME ?>/images/icons/css/font-awesome.css" rel="stylesheet">

</head>
<body>
    <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                    <i class="icon-reorder shaded"></i></a><a class="brand" href="<?= HOME ?>"><?= SITENAME ?> </a>
                    <!-- /.nav-collapse -->
                </div>
            </div>
            <!-- /navbar-inner -->
        </div>
        <!-- /navbar -->
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="span3">
                        <div class="sidebar">
                            <ul class="widget widget-menu unstyled">
                                <li><a href="#"><i class="menu-icon icon-paste"></i>Registro</a>
                                    <ul id="togglePages" class="collapse in unstyled">
                                        <li><a href="index.php?sys=boletim/index"><i class="icon-arrow-right"></i>Boletim de Ocorrência</a></li>
                                        <li><a href="index.php?sys=crimes/index"><i class="icon-arrow-right"></i>Crimes</a></li>
                                        <li><a href="index.php?sys=depoimentos/index"><i class="icon-arrow-right"></i>Depoimentos</a></li>
                                        <li><a href="index.php?sys=desaparecidos/index"><i class="icon-arrow-right"></i>Desaparecidos</a></li>
                                        <li><a href="index.php?sys=investigacao/index"><i class="icon-arrow-right"></i>Investigação</a></li>
                                        <li><a href="index.php?sys=locais/index"><i class="icon-arrow-right"></i>Locais Conhecidos</a></li>
                                        <li><a href="index.php?sys=oficiais/index"><i class="icon-arrow-right"></i>Oficiais</a></li>
                                        <li><a href="index.php?sys=operacoes/index"><i class="icon-arrow-right"></i>Operações</a></li>
                                        <li><a href="index.php?sys=pessoas/index"><i class="icon-arrow-right"></i>Pessoas</a></li>
                                        <li><a href="index.php?sys=unidades/index"><i class="icon-arrow-right"></i>Unidades Policiais</a></li>
                                    </ul>
                                </li>
                            </ul>
                            <!--/.widget-nav-->
                            
                            
                            <ul class="widget widget-menu unstyled">
                                <li><a href="#"><i class="menu-icon icon-search"></i>Consultas</a>
                                    <ul id="togglePages" class="collapse in unstyled">
                                        <li><a href="#"><i class="icon-arrow-right"></i>Nothing</a></li>
                                    </ul>
                                </li>
                                </li>
                            </ul>
                            <!--/.widget-nav-->
                        </div>
                        <!--/.sidebar-->
                    </div>
                    <!--/.span3-->
                    <div class="span9">
                        <?php 
                            if (!empty($getView)):
                                $includepatch = __DIR__ . '/views/' . strip_tags(trim($getView) . '.inc.php');
                            else:
                                $includepatch = __DIR__ . '/views/' . 'boletim.inc.php';
                            endif;

                            if (file_exists($includepatch)):
                                require_once($includepatch);
                            else:
                                header('Location: index.php?sys=home');
                            endif;
                        ?>
                    </div>
                    <!--/.span9-->
                </div>
            </div>
            <!--/.container-->
        </div>
        <!--/.wrapper-->
        <div class="footer">
            <div class="container">
                <b class="copyright">&copy; 2018 <?= SITENAME ?> - </b>Todos os direitos reservados.
            </div>
        </div>
        <script src="<?= HOME ?>/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="<?= HOME ?>/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
        <script src="<?= HOME ?>/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?= HOME ?>/scripts/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="<?= HOME ?>/scripts/jquery.form.js" type="text/javascript"></script>
        <script src="<?= HOME ?>/scripts/common.js" type="text/javascript"></script>

    </body>
    </html>