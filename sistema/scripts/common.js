﻿$(document).ready(function () {
    if($('.datatable-1').length>0){
        $('.datatable-1').dataTable({
            "order": [[ 3, "asc" ]]

        });
        $('.dataTables_paginate').addClass('btn-group datatable-pagination');
        $('.dataTables_paginate > a').wrapInner('<span />');
        $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
        $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
    }

    $('.form').not('.ajax_off').submit(function (e) {
        e.preventDefault();
        var form = $(this);
        var callback = form.find('input[name="callback"]').val();
        var callback_action = form.find('input[name="callback_action"]').val();
        
        form.ajaxSubmit({
            url: './_ajax/' + callback + '.ajax.php',
            data: {callback_action: callback_action},
            dataType: 'json',
            success: function (data) {
            	alert(data.trigger);
            },
            error: function (e){
           		alert("Erro ao enviar os dados"); 
            }
        });
        return false;
    });

});
