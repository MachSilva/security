<?php 
	
$Read = new Read;
$Create = new Create;

$DataID = filter_input(INPUT_GET, 'cpf', FILTER_VALIDATE_INT);

print($DataID);
/*if($DataID):
	$Read->ExeRead(TB_BOLETIM, "WHERE numero_bo = id", "id={$DataID}");
	if($Read->getResult()):
		$FormData = array_map('htmlspecialchars', $Read->getResult()[0]);
		extract($FormData);
	else: ?>
		<script type="text/javascript">
			window.alert("Você tentou editar um boletim que não existe!");
		</script> 
	<?	header('Location: index.php')
		//die();
	endif;
else:
	$DataCreate = [];
	$Create->ExeCreate(TB_BOLETIM, $DataCreate);
	header('Location: index.php?sys=boletim&id=' . $Create->getResult());
endif;*/


?>

<dir class="content">
	
	<div class="module">
		<div class="module-head"><h2>Cadastro de Pessoas</h2></div>
		<div class="module-body">
<!-- Special version of Bootstrap that only affects content wrapped in .bootstrap-iso -->
<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" /> 

<!-- Inline CSS based on choices in "Settings" tab -->
<style>.bootstrap-iso .formden_header h2, .bootstrap-iso .formden_header p, .bootstrap-iso form{font-family: Arial, Helvetica, sans-serif; color: black}.bootstrap-iso form button, .bootstrap-iso form button:hover{color: white !important;} .asteriskField{color: red;}</style>

<!-- HTML Form (wrapped in a .bootstrap-iso div) -->
<div class="bootstrap-iso">
 <div class="container-fluid">
  <div class="row">
   <div class="col-md-6 col-sm-6 col-xs-12">
    <form method="post">
     <div class="form-group ">
      <label class="control-label requiredField" for="cpf">
       CPF
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="cpf" name="cpf" placeholder="012345678901" type="text"/>
     </div>
     <div class="form-group ">
      <label class="control-label requiredField" for="matricula">
       Matr&iacute;cula
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="matricula" name="matricula" placeholder="10000" type="text"/>
     </div>
     <div class="form-group ">
      <label class="control-label requiredField" for="patente">
       Patente
       <span class="asteriskField">
        *
       </span>
      </label>
      <select class="select form-control" id="patente" name="patente">
       <option value="AGENTE">
        AGENTE
       </option>
       <option value="DELEGADO">
        DELEGADO
       </option>
       <option value="INVESTIGADOR">
        INVESTIGADOR
       </option>
       <option value="SUPERINTENDENTE">
        SUPERINTENDENTE
       </option>
       <option value="SUPERINTENDENTE GERAL">
        SUPERINTENDENTE GERAL
       </option>
       <option value="SUPERINTENDENTE REGIONAL">
        SUPERINTENDENTE REGIONAL
       </option>
      </select>
     </div>
     <div class="form-group ">
      <label class="control-label requiredField" for="unidade">
       C&oacute;digo da Unidade
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="unidade" name="unidade" placeholder="1" type="text"/>
     </div>
     <div class="form-group ">
      <label class="control-label requiredField" for="senha">
       Senha
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="senha" name="senha" type="password"/>
     </div>
     <div class="form-group">
      <div>
       <button class="btn btn-primary " name="submit" type="submit">
        Salvar
       </button>
      </div>
     </div>
    </form>
   </div>
  </div>
 </div>
</div>

		</div>
	</div>

</dir>