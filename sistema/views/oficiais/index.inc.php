<?php 

$Read = new Read();
$Read->FullRead("SELECT * FROM ((SELECT * FROM oficial) AS O JOIN pessoa AS P ON (O.cpf = P.cpf) JOIN unidade AS U ON (O.unidade = U.id))");

?>

<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>
            Oficiais
            <a class="btn btn-primary" style="float: right;" href="index.php?sys=oficiais/cadastro">Cadastrar Novo</a>
        	</h3>
        </div>
        <div class="module-body table">
            <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped  display" width="100%">
                <thead>
                    <tr>
                        <th>Matrícula</th>
                        <th>Nome</th>
                        <th>Patente</th>
                        <th>Unidade</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        /* Verifica se retornou resultado */
                        if($Read->getResult()):
                            /* Percorre o resultado */
                            foreach($Read->getResult() as $Resultado):
                                /* Transforma cada compo da tabela em uma variavel */
                                extract($Resultado);
                    ?>

                    <tr class="even gradeX">
                        <td><?= $matricula; ?></td>
                        <td><?= $nome_completo; ?></td>
                        <td><?= $patente; ?></td>
                        <td><?= $nome; ?></td>
                    </tr>

                    <?php 
                            endforeach;
                        endif; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>Matrícula</th>
                        <th>Nome</th>
                        <th>Patente</th>
                        <th>Unidade</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <!--/.module-->
</div>
<!--/.content-->