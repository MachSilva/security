<?php 

$Read = new Read();
$Read->FullRead("SELECT cpf, nome_completo, telefone FROM pessoa");

?>

<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>
            Pessoas
            <a class="btn btn-primary" style="float: right;" href="index.php?sys=pessoas/cadastro">Cadastrar Novo</a>
        	</h3>
        </div>
        <div class="module-body table">
            <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped  display" width="100%">
                <thead>
                    <tr>
                        <th>CPF</th>
                        <th>Nome Completo</th>
                        <th>Telefone</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        /* Verifica se retornou resultado */
                        if($Read->getResult()):
                            /* Percorre o resultado */
                            foreach($Read->getResult() as $Resultado):
                                /* Transforma cada compo da tabela em uma variavel */
                                extract($Resultado);
                    ?>

                    <tr class="even gradeX">
                        <td><?= $cpf; ?></td>
                        <td><?= $nome_completo; ?></td>
                        <td><?= $telefone; ?></td>
                    </tr>

                    <?php 
                            endforeach;
                        endif; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>CPF</th>
                        <th>Nome Completo</th>
                        <th>Telefone</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <!--/.module-->
</div>
<!--/.content-->