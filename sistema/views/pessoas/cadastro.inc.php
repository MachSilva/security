<?php 
	
$Read = new Read;
$Create = new Create;

$DataID = filter_input(INPUT_GET, 'cpf', FILTER_VALIDATE_INT);

print($DataID);
/*if($DataID):
	$Read->ExeRead(TB_BOLETIM, "WHERE numero_bo = id", "id={$DataID}");
	if($Read->getResult()):
		$FormData = array_map('htmlspecialchars', $Read->getResult()[0]);
		extract($FormData);
	else: ?>
		<script type="text/javascript">
			window.alert("Você tentou editar um boletim que não existe!");
		</script> 
	<?	header('Location: index.php')
		//die();
	endif;
else:
	$DataCreate = [];
	$Create->ExeCreate(TB_BOLETIM, $DataCreate);
	header('Location: index.php?sys=boletim&id=' . $Create->getResult());
endif;*/


?>

<dir class="content">
	
	<div class="module">
		<div class="module-head"><h2>Cadastro de Pessoas</h2></div>
		<div class="module-body">

<!-- Special version of Bootstrap that only affects content wrapped in .bootstrap-iso -->
<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" /> 

<!-- Inline CSS based on choices in "Settings" tab -->
<style>.bootstrap-iso .formden_header h2, .bootstrap-iso .formden_header p, .bootstrap-iso form{font-family: Arial, Helvetica, sans-serif; color: black}.bootstrap-iso form button, .bootstrap-iso form button:hover{color: white !important;} .asteriskField{color: red;}</style>

<!-- HTML Form (wrapped in a .bootstrap-iso div) -->
<div class="bootstrap-iso">
 <div class="container-fluid">
  <div class="row">
   <div class="col-md-6 col-sm-6 col-xs-12">
    <form method="post">
     <div class="form-group ">
      <label class="control-label requiredField" for="cpf">
       CPF
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="cpf" name="cpf" placeholder="012345678901" type="text"/>
     </div>
     <div class="form-group ">
      <label class="control-label requiredField" for="nome_completo">
       Nome Completo
       <span class="asteriskField">
        *
       </span>
      </label>
      <input class="form-control" id="nome_completo" name="nome_completo" placeholder="Jo&atilde;o da Silva" type="text"/>
     </div>
     <div class="form-group ">
      <label class="control-label " for="estado_civil">
       Estado Civil
      </label>
      <select class="select form-control" id="estado_civil" name="estado_civil">
       <option value="SOLTEIRO">
        SOLTEIRO
       </option>
       <option value="CASADO">
        CASADO
       </option>
       <option value="UNIAO ESTAVEL">
        UNIAO ESTAVEL
       </option>
       <option value="VIUVO">
        VIUVO
       </option>
       <option value="DIVORCIADO">
        DIVORCIADO
       </option>
       <option value="OUTRO">
        OUTRO
       </option>
      </select>
     </div>
     <div class="form-group ">
      <label class="control-label " for="telefone">
       Telefone
      </label>
      <input class="form-control" id="telefone" name="telefone" placeholder="+55 67 9 8812 3456" type="text"/>
     </div>
     <div class="form-group ">
      <label class="control-label " for="rg_estado">
       RG Estado de Emiss&atilde;o
      </label>
      <input class="form-control" id="rg_estado" name="rg_estado" placeholder="MS" type="text"/>
     </div>
     <div class="form-group ">
      <label class="control-label " for="data_nascimento">
       Data de Nascimento
      </label>
      <input class="form-control" id="data_nascimento" name="data_nascimento" placeholder="1999-01-12" type="text"/>
     </div>
     <div class="form-group ">
      <label class="control-label " for="rg_orgao">
       RG &Oacute;rg&atilde;o Emissor
      </label>
      <input class="form-control" id="rg_orgao" name="rg_orgao" placeholder="SSP" type="text"/>
      <span class="help-block" id="hint_rg_orgao">
       Exemplo: SSP (Secretaria de Seguran&ccedil;a P&uacute;blica)
      </span>
     </div>
     <div class="form-group ">
      <label class="control-label " for="number2">
       RG N&uacute;mero
      </label>
      <input class="form-control" id="number2" name="number2" placeholder="0123456789" type="text"/>
     </div>
     <div class="form-group ">
      <label class="control-label " for="nacionalidade">
       Nacionalidade
      </label>
      <input class="form-control" id="nacionalidade" name="nacionalidade" placeholder="BR" type="text"/>
     </div>
     <div class="form-group ">
      <label class="control-label " for="name3">
       Naturalidade
      </label>
      <input class="form-control" id="name3" name="name3" placeholder="BR" type="text"/>
     </div>
     <div class="form-group">
      <div>
       <button class="btn btn-primary " name="submit" type="submit">
        Salvar
       </button>
      </div>
     </div>
    </form>
   </div>
  </div>
 </div>
</div>


		</div>
	</div>

</dir>