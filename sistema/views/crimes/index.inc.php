<?php 

$Read = new Read();
$Read->FullRead("SELECT * FROM (SELECT * FROM crime) AS c JOIN pessoa AS p ON (c.cpf = p.cpf) ORDER BY data_hora DESC");

?>

<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>
            Crimes
            <a class="btn btn-primary" style="float: right;" href="index.php?sys=crimes/cadastro">Cadastrar Novo</a>
        	</h3>
        </div>
        <div class="module-body table">
            <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped  display" width="100%">
                <thead>
                    <tr>
                        <th>CPF</th>
                        <th>Transgressor</th>
                        <th>Momento</th>
                        <th>Descrição do Fato</th>
                        <th>Ação</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        /* Verifica se retornou resultado */
                        if($Read->getResult()):
                            /* Percorre o resultado */
                            foreach($Read->getResult() as $Resultado):
                                /* Transforma cada compo da tabela em uma variavel */
                                extract($Resultado);
                    ?>

                    <tr class="even gradeX">
                        <td><?= $cpf; ?></td>
                        <td><?= $nome_completo; ?></td>
                        <td><?= $data_hora; ?></td>
                        <td><?= $descricao; ?></td>
                        <td><a href="index.php?sys=crimes/cadastro&id=<?= $cpf; ?>&data=<?= $data_hora ?>"><i class="icon-edit"></i></a> | <a href="index.php?sys=crimes/cadastro&id=<?= $cpf; ?>&data=<?= $data_hora ?>"><i class="icon-trash"></i></a></td>
                    </tr>

                    <?php 
                            endforeach;
                        endif; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>CPF</th>
                        <th>Transgressor</th>
                        <th>Momento</th>
                        <th>Descrição do Fato</th>
                        <th>Ação</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <!--/.module-->
</div>
<!--/.content-->