<?php 

$Read = new Read;
$Create = new Create;

$DataID  = filter_input(INPUT_GET, 'cpf', FILTER_VALIDATE_INT);
$DataID2 = filter_input(INPUT_GET, 'data', FILTER_DEFAULT);

if($DataID):
    $Read->ExeRead(TB_CRIMES, "WHERE data_hora = :id2 AND cpf = :id", "id={$DataID}, id2={$DataID2}");

    if($Read->getResult()):
        extract($FormData);
    else: ?>
        <script type="text/javascript">
            window.alert("Você tentou editar um crime que não existe!");
        </script> 
        <?	header('Location: index.php')
    endif;
else:
    header('Location: index.php?sys=crimes/index');
endif;


 ?>

 <dir class="content">

   <div class="module">
      <div class="module-head"><h2>Registro de Crime</h2></div>
      <div class="module-body">
         <!-- Special version of Bootstrap that only affects content wrapped in .bootstrap-iso -->
         <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" /> 

         <!-- Inline CSS based on choices in "Settings" tab -->
         <style>.bootstrap-iso .formden_header h2, .bootstrap-iso .formden_header p, .bootstrap-iso form{font-family: Arial, Helvetica, sans-serif; color: black}.bootstrap-iso form button, .bootstrap-iso form button:hover{color: white !important;} .asteriskField{color: red;}</style>

         <!-- HTML Form (wrapped in a .bootstrap-iso div) -->
         <div class="bootstrap-iso">
             <div class="container-fluid">
              <div class="row">
               <div class="col-md-12 col-sm-12 col-xs-12">
                <form method="post" class="form">
                 <input type="hidden" name="callback" value="crime">
                 <input type="hidden" name="callback_action" value="editar">
                 <div class="form-group ">
                  <label class="control-label requiredField" for="cpf">
                   CPF do transgressor
                   <span class="asteriskField">
                    *
                </span>
            </label>
            <input class="form-control" id="cpf" name="cpf" placeholder="012345678901" value="<?= $cpf; ?>" type="text"/>
            <span class="help-block" id="hint_cpf">
               O CPF precisa estar cadastrado no sistema. Se voc&ecirc; n&atilde;o cadastrou, fa&ccedil;a isso agora em link
           </span>
       </div>
       <div class="form-group ">
          <label class="control-label requiredField" for="data_hora" value="<?= $data_hora; ?>">
           Momento do fato
           <span class="asteriskField">
            *
        </span>
    </label>
    <input class="form-control" id="data_hora" name="data_hora" <?= $data_hora; ?> placeholder="2018-01-10 15:36" type="text"/>
</div>
<div class="form-group ">
  <label class="control-label requiredField" for="descricao">
   Descri&ccedil;&atilde;o do crime
   <span class="asteriskField">
    *
</span>
</label>
<textarea class="form-control" cols="40" id="descricao" name="descricao" rows="10"><?= $descricao; ?></textarea>
</div>
<div class="form-group">
  <div>
   <button class="btn btn-primary " name="submit" type="submit">
    Salvar
</button>
</div>
</div>
</form>
</div>
</div>
</div>
</div>

</div>
</div>

</dir>
