<?php 

$Read = new Read();
$Read->FullRead("SELECT * FROM boletim");

?>

<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>
            Resultado Pesquisa
            <a class="btn btn-primary" style="float: right;" href="index.php?sys=boletim/cadastro">Cadastrar Novo</a>
        	</h3>
        </div>
        <div class="module-body table">
            <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped  display" width="100%">
                <thead>
                    <tr>
                        <th>Numero Boletim</th>
                        <th>Oficial</th>
                        <th>Data Fato</th>
                        <th>Data Atendimento</th>
                        <th>Local</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        /* Verifica se retornou resultado */
                        if($Read->getResult()):
                            /* Percorre o resultado */
                            foreach($Read->getResult() as $Resultado):
                                /* Transforma cada compo da tabela em uma variavel */
                                extract($Resultado);
                    ?>

                    <tr class="even gradeX">
                        <td><?= $numero_bo; ?></td>
                        <td><?= $oficial; ?></td>
                        <td><?= $data_fato; ?></td>
                        <td class="center"><?= $data_atendimento; ?></td>
                        <td class="center"><?= $local; ?></td>
                    </tr>

                    <?php 
                            endforeach;
                        endif; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>Numero Boletim</th>
                        <th>Oficial</th>
                        <th>Data Fato</th>
                        <th>Data Atendimento</th>
                        <th>Local</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <!--/.module-->
</div>
<!--/.content-->