<?php 

$Read = new Read();

/* 
    Aqui coloque o comando SQL que deseja execultar 
    O Sistema já implementato ultiliza de Parce String, Exemplo
    Quando damos WHERE id = 10 AND nome = "joao", podemos trocar os valores operador "="
    para a seguinte sintax, WHERE id = :id AND nome = :pessoa, onde em outra variavel
    podemos fazer uma string com a seguinte configuração
    $ParceString = "id=10&nome='pessoa'"

    Isso foi feito para poder filtrar algumas coisas no sistema, é apenas uma estilização para produtividade
*/ 
/*$id = 10;
$valor = true;

$Query = "";
$ParceString = "id={$id}&valor={$valor}";*/

$Read->FullRead("SELECT * FROM boletim");

?>

<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>
            Resultado Pesquisa</h3>
        </div>
        <div class="module-body table">
            <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped  display" width="100%">
                <thead>
                    <tr>
                        <th>Numero Boletim</th>
                        <th>Oficial</th>
                        <th>Data Fato</th>
                        <th>Data Atendimento</th>
                        <th>Local</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        /* Verifica se retornou resultado */
                        if($Read->getResult()):
                            /* Percorre o resultado */
                            foreach($Read->getResult() as $Resultado):
                                /* Transforma cada compo da tabela em uma variavel */
                                extract($Resultado);
                    ?>

                    <tr class="even gradeX">
                        <td><?= $numero_bo; ?></td>
                        <td><?= $oficial; ?></td>
                        <td><?= $data_fato; ?></td>
                        <td class="center"><?= $data_atendimento; ?></td>
                        <td class="center"><?= $local; ?></td>
                    </tr>

                    <?php 
                            endforeach;
                        endif; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>Numero Boletim</th>
                        <th>Oficial</th>
                        <th>Data Fato</th>
                        <th>Data Atendimento</th>
                        <th>Local</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <!--/.module-->
</div>
<!--/.content-->