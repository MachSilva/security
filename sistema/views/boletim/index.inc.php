<?php 

$Read = new Read();
$Read->FullRead("SELECT * FROM ((SELECT * FROM boletim) AS B JOIN oficial AS O ON (B.oficial = O.matricula) JOIN pessoa AS P ON (O.cpf = P.cpf) JOIN local AS L ON (B.local = L.id))");

?>

<div class="content">
    <div class="module">
        <div class="module-head">
            <h3>
            Resultado Pesquisa
            <a class="btn btn-primary" style="float: right;" href="index.php?sys=boletim/cadastro">Cadastrar Novo</a>
        	</h3>
        </div>
        <div class="module-body table">
            <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped  display" width="100%">
                <thead>
                    <tr>
                        <th>Número do Boletim</th>
                        <td>Oficial Responsável</td>
                        <th>Descrição</th>
                        <th>Local</th>
                        <th>Data Fato</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        /* Verifica se retornou resultado */
                        if($Read->getResult()):
                            /* Percorre o resultado */
                            foreach($Read->getResult() as $Resultado):
                                /* Transforma cada compo da tabela em uma variavel */
                                extract($Resultado);
                    ?>

                    <tr class="even gradeX">
                        <td><?= $numero_bo; ?></td>
                        <td><?= $nome_completo; ?></td>
                        <td><?= $descricao; ?></td>
                        <td><?= $nome_rua; ?>; <?= $bairro; ?></td>
                        <td><?= $data_fato; ?></td>
                    </tr>

                    <?php 
                            endforeach;
                        endif; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>Número do Boletim</th>
                        <td>Oficial Responsável</td>
                        <th>Descrição</th>
                        <th>Local</th>
                        <th>Data Fato</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <!--/.module-->
</div>
<!--/.content-->