insert into pessoa (cpf, nome_completo) values
	(901, 'João Silva'),
	(902, 'José Oliveira'),
	(903, 'Ana Júlia Pereira'),
	(904, 'Marcelo Pedro'),
	(905, 'Maria Ferreira'),
	(906, 'Dino Sauro'),
	(907, 'Pedro Aparecido'),
	(908, 'Paulo Nascimento');

insert into local values
	(9000, 86777800, 'Limoeiro', 'Rua do Limoeiro'),
	(9001, 86777800, 'Cajazeiras', 'Rua de Cajazeiras');

insert into desaparecido values
	(907, '2017-11-09', 9000);

insert into unidade values
	(1, 'Policia de Softland', 9000);

-- senha md5 para '12345' : 827ccb0eea8a706c4c34a16891f84e7b

insert into oficial values
	(1000, 901, 'AGENTE', 1, '827ccb0eea8a706c4c34a16891f84e7b'),
	(1002, 903, 'AGENTE', 1, '827ccb0eea8a706c4c34a16891f84e7b');

insert into boletim
	(numero_bo, oficial, descricao, data_fato, data_atendimento, local) values
	(2000, 1000, 'Assalto à mão armada', '2018-01-01 23:00', '2018-01-02 07:59', 9000),
	(2001, 1000, 'Desaparecimento', '2017-11-10 21:00', '2017-11-09 12:00', 9000);

insert into boletim_envolve values
	(2001, 907);

insert into crime values
	('2017-12-31 18:36', 902, 'Detido por matar um civil com um tiro a queima roupa'),
	('2017-11-10 10:06', 906, 'Roubo seguido de morte'),
	('2018-01-02 19:36', 908, 'Tentou matar a ex-mulher');

insert into crime_natureza values
	('HOMICIDIO', '2017-12-31 18:36', 902),
	('CONSUMADO', '2017-12-31 18:36', 902),
	('HOMICIDIO', '2018-01-02 19:36', 908),
	('TENTADO'  , '2018-01-02 19:36', 908),
	('ROUBO'    , '2017-11-10 10:06', 906),
	('HOMICIDIO', '2017-11-10 10:06', 906),
	('CONSUMADO', '2017-11-10 10:06', 906);

insert into investigacao (id, numero_bo, instaurado_por, data_inicio) values
	(3000, 2001, 1000, '2017-11-17'),
	(3001, 2000, 1002, '2018-03-18');

insert into depoimento
	(pessoa, responsavel, investigacao, data_hora, depoimento)
	values
	(905, 1002, 3001, '2018-03-19 11:00', 'O suspeito planejou tudo. Esse falou bem alto que este era o primeiro dos seis...');
