create schema security;

use security;

create table pessoa (
    cpf             bigint(11) zerofill,
    rg_estado       varchar(2),
    rg_orgao        varchar(12),
    rg_numero       bigint(16),
    nome_completo   varchar(64) not null,
    nacionalidade   varchar(2),
    naturalidade    varchar(2),
    telefone        varchar(16),
    estado_civil    enum('SOLTEIRO', 'CASADO', 'UNIAO ESTAVEL', 'VIUVO', 'DIVORCIADO', 'OUTRO'),
    data_nascimento date,

    primary key (cpf),
    unique key (rg_estado, rg_orgao, rg_numero)
);

create table local (
    id          bigint auto_increment,
    cep         bigint(8) zerofill not null, # 12345-678
    bairro      varchar(32) not null,
    nome_rua    varchar(64),

    primary key (id)
);

create table unidade (
    id      bigint auto_increment,
    nome    varchar(64),
    local   bigint,

    primary key (id),
    foreign key (local) references local(id) on update no action on delete no action
);

create table oficial (
    matricula   bigint,
    cpf         bigint(11) zerofill,
    patente     enum('AGENTE', 'DELEGADO', 'INVESTIGADOR', 'SUPERINTENDENTE', 'SUPERINTENDENTE GERAL', 'SUPERINTENDENTE REGIONAL') not null,
    unidade     bigint,
    senha       varchar(255) not null,

    primary key (matricula),
    foreign key (cpf) references pessoa(cpf) on update cascade on delete cascade,
    foreign key (unidade) references unidade(id) on update no action on delete no action
);

create table boletim (
    numero_bo           bigint auto_increment,
    oficial             bigint,
    descricao           text not null,
    data_fato           datetime not null,
    data_atendimento    datetime not null,
    envolvidos_nao_identificados text,
    pendente            boolean,
    local               bigint,

    primary key (numero_bo),
    foreign key (oficial) references oficial(matricula) on update cascade on delete no action,
    foreign key (local) references local(id) on update cascade on delete no action
);

create table desaparecido (
    cpf                 bigint(11) zerofill,
    desde_data          date not null,
    visto_por_ultimo    bigint,

    primary key (cpf),
    foreign key (visto_por_ultimo) references local(id) on update cascade on delete no action
);

create table investigacao (
    id              bigint auto_increment,
    numero_bo       bigint,
    instaurado_por  bigint,
    data_fim        date,
    data_inicio     date not null,

    primary key (id),
    foreign key (numero_bo) references boletim(numero_bo) on update cascade on delete no action,
    foreign key (instaurado_por) references oficial(matricula) on update cascade on delete no action
);

create table operacao (
    nome        varchar(64),
    comandante  bigint,
    data_inicio date not null,
    data_fim    date,

    primary key (nome),
    foreign key (comandante) references oficial(matricula) on update cascade on delete no action
);

create table depoimento (
    id              bigint auto_increment,
    pessoa          bigint(11) zerofill,
    responsavel     bigint not null,
    investigacao    bigint not null,
    data_hora       datetime not null,
    depoimento      text not null,

    primary key (id),
    foreign key (pessoa) references pessoa(cpf) on update cascade on delete no action,
    foreign key (responsavel) references oficial(matricula) on update cascade on delete no action,
    foreign key (investigacao) references investigacao(id) on update cascade on delete no action
);

create table crime (
    data_hora   datetime,
    cpf         bigint(11) zerofill,
    descricao   text not null,

    primary key (data_hora, cpf),
    foreign key (cpf) references pessoa(cpf) on update cascade on delete cascade
);

create table natureza_ocorrencia (
    nome    varchar(64) check (nome like upper(nome)),

    primary key (nome)
);

create table pessoa_reside_em (
    pessoa              bigint(11) zerofill,
    local               bigint,
    data_da_informacao  timestamp not null,

    primary key (pessoa, local),
    foreign key (pessoa) references pessoa(cpf) on update cascade on delete cascade,
    foreign key (local) references local(id) on update no action on delete no action
);

create table oficial_operacao (
    oficial     bigint,
    operacao    varchar(64),

    primary key (oficial, operacao),
    foreign key (oficial) references oficial(matricula) on update cascade on delete cascade,
    foreign key (operacao) references operacao(nome) on update cascade on delete cascade
);

create table oficial_investigacao (
    oficial         bigint,
    investigacao    bigint,

    primary key (oficial, investigacao),
    foreign key (oficial) references oficial(matricula) on update cascade on delete cascade,
    foreign key (investigacao) references investigacao(id) on update cascade on delete cascade
);

create table operacao_local (
    operacao    varchar(64),
    local       bigint,

    primary key (operacao, local),
    foreign key (operacao) references operacao(nome) on update cascade on delete cascade,
    foreign key (local) references local(id) on update cascade on delete no action
);


create table boletim_envolve (
    boletim     bigint,
    pessoa      bigint(11) zerofill,

    primary key (boletim, pessoa),
    foreign key (boletim) references boletim(numero_bo) on update cascade on delete cascade,
    foreign key (pessoa) references pessoa(cpf) on update cascade on delete no action
);

create table boletim_natureza (
    boletim     bigint,
    natureza    varchar(64),

    primary key (boletim, natureza),
    foreign key (boletim) references boletim(numero_bo) on update cascade on delete cascade,
    foreign key (natureza) references natureza_ocorrencia(nome) on update cascade on delete no action
);

create table crime_natureza (
    natureza            varchar(64),
    crime_data_hora     datetime,
    crime_cpf           bigint(11) zerofill,

    primary key (natureza, crime_data_hora, crime_cpf),
    foreign key (natureza) references natureza_ocorrencia(nome) on update cascade on delete no action,
    foreign key (crime_data_hora, crime_cpf) references crime(data_hora, cpf) on update cascade on delete cascade
);

-- Valores básicos de naturezas / categorias para ocorrências
-- Mais poderão ser adicionados depois

insert into natureza_ocorrencia values
    ('ASSEDIO'),
    ('CONSUMADO'),
    ('DESAPARECIMENTO'),
    ('EXTRAVIO'),
    ('FURTO'),
    ('HOMICIDIO'),
    ('INJURIA'),
    ('PEDOFILIA'),
    ('ROUBO'),
    ('SEQUESTRO'),
    ('TENTADO');
